const Joi = require('joi');

const httpErrorArgsSchema = Joi.object({
  details: Joi
    .array()
    .min(1)
    .required()
    .items(Joi.object({
      message: Joi.string().required(),
      path: Joi.array().items(Joi.string()).required(),
      type: Joi.string().required(),
    })),
  status: Joi
    .number()
    .integer()
    .min(400)
    .max(599)
    .required(),
  root: Joi
    .object()
    .required(),
});

class HttpError extends Error {
  constructor(details, status, root = {}) {
    const { value, error } = httpErrorArgsSchema.validate(
      { details, status, root },
      { stripUnknown: true },
    );
    if (error) throw error;
    super(value.details.map(({ message }) => message).join(', '));
    Object.assign(this, value);

    this.send = this.send.bind(this);
    this.toJSON = this.toJSON.bind(this);
  }

  send(res) {
    res.status(this.status).json(this.toJSON());
  }

  toJSON() {
    return {
      ...this.root,
      errors: this.details,
    };
  }
}

class BadRequestError extends HttpError {
  constructor(details, root = {}) {
    super(details, 400, root);
  }
}

class UnauthorizedError extends HttpError {
  constructor(message = 'Unauthorized', root = {}) {
    super([{ message, path: [], type: 'http.unauthorized' }], 401, root);
  }
}

class ForbiddenError extends HttpError {
  constructor(message = 'Forbidden', root = {}) {
    super([{ message, path: [], type: 'http.forbidden' }], 403, root);
  }
}

class NotFoundError extends HttpError {
  constructor(message = 'Not Found', root = {}) {
    super([{ message, path: [], type: 'http.notFound' }], 404, root);
  }
}

class MethodNotAllowedError extends HttpError {
  constructor(message = 'Method Not Allowed', root = {}) {
    super([{ message, path: [], type: 'http.methodNotAllowed' }], 405, root);
  }
}

class InternalServerError extends HttpError {
  constructor(message = 'Unhandled Error', root = {}) {
    super([{ message, path: [], type: 'http.internalServer' }], 500, root);
  }
}

class NotImplementedError extends HttpError {
  constructor(message = 'Not Implemented', root = {}) {
    super([{ message, path: [], type: 'http.notImplemented' }], 501, root);
  }
}

class ValidationErrorWrapper extends Error {
  constructor(err) {
    super(err.message);
    this._original = err._original;
    this.details = err.details;
  }
}

module.exports = {
  HttpError,

  BadRequestError,
  UnauthorizedError,
  ForbiddenError,
  NotFoundError,
  MethodNotAllowedError,
  InternalServerError,
  NotImplementedError,

  ValidationErrorWrapper,
};
