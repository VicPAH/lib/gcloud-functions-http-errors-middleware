const { HttpError } = require('./errors');

describe('HttpError', () => {
  const details = [{ message: 'Test', path: [], type: 'test' }];
  describe('constructor()', () => {
    // These tests are probably overkill; simply asserting that
    // the schema is being applied is enough
    function buildErrorTest(key, value, desc, message) {
      const args = [details, 400];
      const argsIdx = { details: 0, status: 1, root: 2 }[key];
      args[argsIdx] = value;

      test(`${desc} causes error`, () => {
        expect(() => new HttpError(...args))
          .toThrowError(`"${key}" ${message}`);
      });
    }
    describe('details', () => {
      buildErrorTest('details', undefined, 'undefined', 'is required');
      buildErrorTest('details', null, 'null', 'must be an array');
      buildErrorTest('details', 'test', 'non-array', 'must be an array');
      buildErrorTest('details', [], 'zero-length array', 'must contain at least 1 items');

      test('message must be string', () => {
        expect(() => new HttpError([{ ...details[0], message: null }], 400))
          .toThrowError('"details[0].message" must be a string');
      });
      test('message is required', () => {
        expect(() => new HttpError([{ ...details[0], message: undefined }], 400))
          .toThrowError('"details[0].message" is required');
      });
      test('type must be string', () => {
        expect(() => new HttpError([{ ...details[0], type: null }], 400))
          .toThrowError('"details[0].type" must be a string');
      });
      test('type is required', () => {
        expect(() => new HttpError([{ ...details[0], type: undefined }], 400))
          .toThrowError('"details[0].type" is required');
      });
      test('path must be array', () => {
        expect(() => new HttpError([{ ...details[0], path: null }], 400))
          .toThrowError('"details[0].path" must be an array');
      });
      test('path is required', () => {
        expect(() => new HttpError([{ ...details[0], path: undefined }], 400))
          .toThrowError('"details[0].path" is required');
      });
      test('path must be array of strings', () => {
        expect(() => new HttpError([{ ...details[0], path: [null] }], 400))
          .toThrowError('"details[0].path[0]" must be a string');
      });
    });
    describe('status', () => {
      buildErrorTest('status', undefined, 'undefined', 'is required');
      buildErrorTest('status', null, 'null', 'must be a number');
      buildErrorTest('status', 'test', 'non-number', 'must be a number');
      buildErrorTest('status', 399, 'less than 400', 'must be greater than or equal to 400');
      buildErrorTest('status', 600, 'larger than 599', 'must be less than or equal to 599');

      test('sets status on error object', () => {
        expect(new HttpError(details, 400).status).toBe(400);
      });
    });
    describe('root', () => {
      buildErrorTest('root', null, 'null', 'must be of type object');
      buildErrorTest('root', 'test', 'non-object', 'must be of type object');

      test('adds items to toJSON', () => {
        const root = { testkey: 'testval' };
        expect(new HttpError(details, 400, root).toJSON())
          .toEqual(expect.objectContaining(root));
      });
    });
  });
});
