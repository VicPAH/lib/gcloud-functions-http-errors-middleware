const debug = require('debug');

const { HttpError, InternalServerError } = require('./errors');

const LOG = debug('middleware').extend('httpError');
const LOG_ERR = LOG.extend('error');

exports.handleHttpErrors = async function handleHttpErrors(req, res, next) {
  try {
    await next();
  } catch (err) {
    if (err instanceof HttpError) {
      err.send(res);
    } else {
      LOG_ERR('Unhandled error:', err);
      new InternalServerError().send(res);
    }
  }
};
