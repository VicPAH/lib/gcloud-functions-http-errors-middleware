const { handleHttpErrors } = require('.');
const { NotFoundError } = require('./errors');

const mockRes = () => {
  const v = {
    status: jest.fn(() => v),
    json: jest.fn(() => v),
  };
  return v;
};

describe('handleHttpErrors', () => {
  test('handles NotFoundError', async () => {
    const res = mockRes();
    const next = async () => {
      throw new NotFoundError();
    };
    await handleHttpErrors(null, res, next);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledWith({
      errors: [{ message: 'Not Found', path: [], type: 'http.notFound' }],
    });
  });
  test('handles other errors as HTTP 500', async () => {
    const res = mockRes();
    const next = async () => {
      throw new Error('bad error');
    };
    await handleHttpErrors(null, res, next);
    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({
      errors: [{ message: 'Unhandled Error', path: [], type: 'http.internalServer' }],
    });
  });
});
